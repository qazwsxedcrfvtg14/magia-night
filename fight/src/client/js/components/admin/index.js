import Admin from './admin';
import match from './match';
import matches from './matches';
import competition from './competition';

export default {
  index: Admin,
  match,
  matches,
  competition
};
