import Vue from 'vue';
import html from './matches.pug';
import toastr from 'toastr';
import _ from 'lodash';
import store, {getUser} from 'js/store';

export default Vue.extend({
  data () {
    return {
      matches: null,
      newDesc: '八進七 $A v.s. $D'
    };
  },
  template: html,
  ready () {
    this.timer = setTimeout(this.updateData, 0);
  },
  beforeDestroy () {
    clearTimeout(this.timer);
    this.timer = null;
  },
  methods: {
    async updateData () {
      clearTimeout(this.timer);
      try {
        const data = (await this.$http.get(`/admin/match/list`)).data;
        // console.log(data);
        if (JSON.stringify(data) !== JSON.stringify(this.competitions)) {
          this.matches = data;
        }
      } catch (e) {
        if ('body' in e) { toastr.error(e.body); } else console.log(e);
      }
      if (!_.isNil(this.timer)) { this.timer = setTimeout(this.updateData, 10000); }
    }
  },
  filters: {
  },
  store,
  vuex: {
    getters: {
      user: getUser
    }
  }
});
