import Vue from 'vue';
import html from './competition.pug';
import toastr from 'toastr';
import _ from 'lodash';
import store, {getUser} from 'js/store';

export default Vue.extend({
  data () {
    return {
      competitions: null,
      newDesc: '八進七 $A v.s. $D'
    };
  },
  template: html,
  ready () {
    this.timer = setTimeout(this.updateData, 0);
  },
  beforeDestroy () {
    clearTimeout(this.timer);
    this.timer = null;
  },
  methods: {
    async updateData () {
      clearTimeout(this.timer);
      try {
        const data = (await this.$http.get(`/admin/competition/list`)).data;
        // console.log(data);
        if (JSON.stringify(data) !== JSON.stringify(this.competitions)) {
          this.competitions = data;
        }
      } catch (e) {
        if ('body' in e) { toastr.error(e.body); } else console.log(e);
      }
      if (!_.isNil(this.timer)) { this.timer = setTimeout(this.updateData, 10000); }
    },
    async clickCom (id) {
      try {
        const data2 = (await this.$http.post(`/admin/competition/active`, {
          id: id
        })).data;
        if (data2.status !== 'success') {
          throw Error('Failed!');
        }
        await this.updateData();
      } catch (e) {
        if ('body' in e) { toastr.error(e.body); } else console.log(e);
      }
    },
    async clickNewDesc () {
      try {
        const data2 = (await this.$http.post(`/admin/competition/add`, {
          desc: this.newDesc
        })).data;
        if (data2.status !== 'success') {
          throw Error('Failed!');
        }
        await this.updateData();
      } catch (e) {
        if ('body' in e) { toastr.error(e.body); } else console.log(e);
      }
    }
  },
  filters: {
  },
  store,
  vuex: {
    getters: {
      user: getUser
    }
  }
});
