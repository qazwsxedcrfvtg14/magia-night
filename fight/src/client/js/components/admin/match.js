import Vue from 'vue';
import html from './match.pug';
import toastr from 'toastr';
import _ from 'lodash';
import store, {getUser} from 'js/store';

export default Vue.extend({
  data () {
    return {
      players: null,
      result: null,
      attacker: null,
      defender: null
    };
  },
  template: html,
  ready () {
    this.timer = setTimeout(this.updateData, 0);
  },
  beforeDestroy () {
    clearTimeout(this.timer);
    this.timer = null;
  },
  methods: {
    async updateData () {
      clearTimeout(this.timer);
      try {
        const data = (await this.$http.get(`/admin/match/player`)).data;
        // console.log(data);
        if (JSON.stringify(data) !== JSON.stringify(this.players)) {
          this.players = data;
          Vue.nextTick(() => {
            $('#attacker').dropdown();
            $('#defender').dropdown();
          });
        }
      } catch (e) {}
      if (!_.isNil(this.timer)) { this.timer = setTimeout(this.updateData, 10000); }
    },
    async clickSubmit () {
      const attacker = this.attacker;
      const defender = this.defender;
      if (!attacker) {
        toastr.error('進攻方為空!');
        return;
      }
      if (!defender) {
        toastr.error('防守方為空!');
        return;
      }
      $('#attacker').dropdown('clear');
      $('#defender').dropdown('clear');
      try {
        const data2 = (await this.$http.post(`/admin/match/new`, {
          attacker: attacker,
          defender: defender
        })).data;
        if (data2.status !== 'success') {
          throw Error('Failed!');
        }
        this.result = data2.key;
      } catch (e) {
        if ('body' in e) { toastr.error(e.body); } else console.log(e);
        this.attacker = attacker;
        this.defender = defender;
        Vue.nextTick(() => {
          $('#attacker').dropdown();
          $('#defender').dropdown();
        });
      }
    }
  },
  filters: {
  },
  store,
  vuex: {
    getters: {
      user: getUser
    }
  }
});
