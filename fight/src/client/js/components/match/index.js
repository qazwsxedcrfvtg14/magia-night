import Vue from 'vue';
import html from './index.pug';
import './index.css';
import toastr from 'toastr';
import _ from 'lodash';
import store, {getUser} from 'js/store';

export default Vue.extend({
  data () {
    return {
      match: null,
      points: [{value: 0}, {value: 0}, {value: 0}, {value: 0}, {value: 0}]
    };
  },
  template: html,
  ready () {
    this.timer = setTimeout(this.updateData, 0);
  },
  beforeDestroy () {
    clearTimeout(this.timer);
    this.timer = null;
  },
  methods: {
    async changeStatus (target) {
      try {
        const data2 = (await this.$http.post(`/match/change`, {
          key: this.$route.params.key,
          target: target
        })).data;
        if (data2.status !== 'success') {
          throw Error('Failed!');
        }
      } catch (e) {
        if ('body' in e) { toastr.error(e.body); } else console.log(e);
        return;
      }
      await this.updateData();
    },
    async updateData () {
      clearTimeout(this.timer);
      try {
        const data = (await this.$http.post(`/match/now`, {
          key: this.$route.params.key
        })).data;
        const match = {};
        match.internal_status = data.status;
        match.desc = data.desc;
        const atk = data.attacker.meta.name;
        const def = data.defender.meta.name;
        match.desc = match.desc.replace('$A', atk);
        match.desc = match.desc.replace('$D', def);
        match.isAttacker = data.isAttacker;
        match.showGreenButton = false;
        match.showRedButton = false;
        match.showBlueButton = false;
        if (data.isAttacker) {
          if (data.status === 'create') {
            match.status = '等待階段';
            match.someText = '請耐心等待';
            this.match = match;
            this.changeStatus('ready');
            return;
          } else if (data.status === 'ready') {
            match.status = '準備開始階段';
            match.someText = '請開始錄影，然後點擊戰鬥開始';
            match.showGreenButton = true;
            match.buttonText = '戰鬥開始';
          } else if (data.status === 'start') {
            match.status = '戰鬥進行中';
            match.someText = '請勿停止錄影，當戰鬥結束後請點擊戰鬥結束';
            match.showRedButton = true;
            match.buttonText = '戰鬥結束';
          } else if (data.status === 'finish') {
            match.status = '戰鬥已結束';
            match.someText = '請停止錄影，並上傳錄影紀錄';
            match.showBlueButton = true;
            match.buttonText = '回首頁';
          } else if (data.status === 'scored') {
            match.status = '計分完成';
            match.someText = '請回到首頁';
            match.showBlueButton = true;
            match.buttonText = '回首頁';
          }
        } else {
          if (data.status === 'create') {
            match.status = '等待階段';
            match.someText = '請耐心等待';
          } else if (data.status === 'ready') {
            match.status = '準備開始階段';
            match.someText = '請耐心等待';
          } else if (data.status === 'start') {
            match.status = '戰鬥進行中';
            match.someText = '請耐心等待';
          } else if (data.status === 'finish') {
            match.status = '戰鬥已結束';
            match.someText = '請把錄像內的資訊填入表格中';
            match.showGreenButton = true;
            match.showInput = true;
            match.buttonText = '送出';
          } else if (data.status === 'scored') {
            match.status = '計分完成';
            match.someText = '請回到首頁';
            match.showBlueButton = true;
            match.buttonText = '回首頁';
          }
        }
        this.match = match;
      } catch (e) {
        if ('body' in e) { toastr.error(e.body); } else console.log(e);
        this.match = {};
        this.match.internal_status = 'scored';
        this.match.showButton = true;
        this.match.buttonText = '回首頁';
        return;
      }
      if (!_.isNil(this.timer)) { this.timer = setTimeout(this.updateData, 10000); }
    },
    async clickButton () {
      if (this.match.internal_status === 'ready') {
        this.changeStatus('start');
      } else if (this.match.internal_status === 'start') {
        this.changeStatus('finish');
      } else if (this.match.internal_status === 'finish') {
        if (this.match.isAttacker) {
          this.$router.go({
            name: 'home'
          });
        } else {
          const points = [0, 0, 0, 0, 0];
          for (let i = 0; i < 5; i++) {
            points[i] = this.points[i].value;
          }
          try {
            const data2 = (await this.$http.post(`/match/points`, {
              key: this.$route.params.key,
              points: points
            })).data;
            if (data2.status !== 'success') {
              throw Error('Failed!');
            }
          } catch (e) {
            if ('body' in e) { toastr.error(e.body); } else console.log(e);
          }
          await this.updateData();
        }
      } else if (this.match.internal_status === 'scored') {
        this.$router.go({
          name: 'home'
        });
      }
    }
  },
  filters: {
  },
  store,
  vuex: {
    getters: {
      user: getUser
    }
  }
});
