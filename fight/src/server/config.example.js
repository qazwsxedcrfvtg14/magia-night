import path from 'path';
export default {
  secret: 'aabbccaabbddaaeeff',
  port: 3333,
  dirs: {},
  mongo: {
    url: 'mongodb://localhost/irohafight'
  },
  maxWorkers: 4,
  maxNodeWorkers: 4
};
