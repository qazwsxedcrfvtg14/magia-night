import express from 'express';
import wrap from 'express-async-wrap';
import {requireAdmin} from '/utils';
import Match from '/model/match';
import User from '/model/user';
import Competition from '/model/competition';
import randomString from 'randomstring';
const router = express.Router();

router.post('/new', requireAdmin, wrap(async (req, res) => {
  const act = await Competition.findOne({active: true});
  if (!act) {
    return res.status(401).send(`There is no active competition!`);
  }
  const attacker = await User.findOne({_id: req.body.attacker});
  const defender = await User.findOne({_id: req.body.defender});
  if (!attacker) {
    return res.status(401).send(`Attacker Not Found!`);
  }
  if (!defender) {
    return res.status(401).send(`Defender Not Found!`);
  }
  if (String(attacker._id) === String(defender._id)) {
    return res.status(401).send(`Attacker and Defender are the same!`);
  }
  if (String(attacker._id) === String(req.user._id)) {
    return res.status(401).send(`Attacker and You are the same!`);
  }
  if (String(defender._id) === String(req.user._id)) {
    return res.status(401).send(`Defender and You are the same!`);
  }
  let key = randomString.generate(10);
  while (await Match.findOne({key: key})) {
    key = randomString.generate(10);
  }
  const match = new Match();
  match.status = 'create';
  match.timestamps = [];
  match.timestamps.push({
    status: 'create'
  });
  match.applicant = req.user._id;
  match.attacker = attacker._id;
  match.defender = defender._id;
  match.competition = act;
  match.key = key;
  // TODO: there may have a race condition.
  await match.save();
  res.send({
    status: 'success',
    key: key
  });
}));

router.get('/player', requireAdmin, wrap(async (req, res) => {
  const users = await User.find(
    {
      roles: 'player'
    });
  const result = [];
  for (let user of users) {
    result.push({
      _id: user._id,
      username: user.username,
      meta: user.meta
    });
  }
  res.send(result);
}));

router.get('/list', requireAdmin, wrap(async (req, res) => {
  const matches = await Match.find({applicant: req.user._id})
    .populate('attacker', 'meta')
    .populate('defender', 'meta')
    .populate('competition', 'desc');
  const result = [];
  for (let match of matches) {
    result.push({
      _id: match._id,
      key: match.key,
      attacker: match.attacker,
      defender: match.defender,
      desc: String(match.competition.desc)
        .replace('$A', match.attacker.meta.name)
        .replace('$D', match.defender.meta.name),
      timestamps: match.timestamps,
      status: match.status,
      points: match.points
    });
  }
  res.send(result);
}));

export default router;
