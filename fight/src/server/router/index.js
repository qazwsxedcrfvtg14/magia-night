const routes = [
  'match',
  'admin',
  'time',
  'user'
];

export default function (app) {
  routes.map(x => {
    app.use(`/${x}`, require(`./${x}`).default);
  });
}
