import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const schema = Schema({
  desc: String,
  active: {
    type: Boolean,
    default: false
  }
});

const Competition = mongoose.model('Competition', schema);
export default Competition;
