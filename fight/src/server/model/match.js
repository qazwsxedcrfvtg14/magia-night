import mongoose from 'mongoose';
// import autoIncrement from './autoIncrement';

const Schema = mongoose.Schema;

const schema = Schema({
  key: {
    type: String,
    required: true,
    unique: true
  },
  applicant: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  attacker: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  defender: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  competition: {
    type: Schema.Types.ObjectId,
    ref: 'Competition',
    required: true
  },
  timestamps: [
    {
      status: String,
      time: {
        type: Date,
        default: Date.now
      }
    }
  ],
  status: String,
  points: [Number]
});

// schema.plugin(autoIncrement.plugin, 'Match');
const Match = mongoose.model('Match', schema);
export default Match;
