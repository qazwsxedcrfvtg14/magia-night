# -*- coding: utf-8 -*-
from flask import Flask
from flask_compress import Compress
from flask import request, send_from_directory, render_template
import flask
import json
import uuid
import requests
import re
import os, os.path
import errno
import time
import hashlib
import email.utils as eut
import datetime
import threading

listenPort = 9487

app = Flask(__name__, static_url_path='')
Compress(app)

def init_player():
    global player,lock
    player={}
    with open("result.txt",'r') as fp:
        lines = fp.readlines()
        for line in lines:
            player[line.split(",")[0]]=True
    return

@app.route('/')
@app.route('/index')
def root():
    global player,lock
    l=0
    with lock:
        #if(len(player)>=64):
        #    return "名额已满"
        l=len(player)
    return render_template('index.html', players=l)

@app.route('/reg', methods=['POST']) 
def login():
    global player,lock,file_lock
    py=request.values['id']
    if not re.match("^\w{6,10}$",py):
        return render_template('msg.html', msg="你的朋友码看起来怪怪的")
    cc=request.values['cursedchip']
    if not re.match("^\d+$",cc):
        return render_template('msg.html', msg="你的CC数量看起来怪怪的")
    qq=request.values['qq']
    if not re.match("^\d{5,15}$",qq):
        return render_template('msg.html', msg="你的QQ号看起来怪怪的")
    r=requests.post("http://jp.rika.ren/search/friend_search/_search",json={"query":{"query_string":{"query":"inviteCode:"+py,"lenient":True}},"stored_fields":"*","size":50})
    #print(r.content)
    #print(r.json)
    jso=r.json()
    if(len(jso["hits"]["hits"])!=1):
        return render_template('msg.html', msg="查无此号!")
    id=jso["hits"]["hits"][0]["_id"]
    r=requests.get("http://jp.rika.ren/magica/api/friend/user/"+id)
    jso=r.json()
    point = jso["gameUser"]["riche"]
    if(point!=int(cc)):
        return render_template('msg.html', msg="CC 数量验证失败!")
    with lock:
        if(py in player):
            return render_template('msg.html', msg="报名过了!")
        #if(len(player)>=64):
        #    return render_template('msg.html', msg="你慢了一步，名额满了!")
        player[py]=True
    point = jso["gameUser"]["rankingArenaPoint"]
    print(py,id,cc,qq,point)
    with file_lock:
        f=open("result.txt", "a+")
        f.write(str(py)+","+str(id)+","+str(cc)+","+str(qq)+","+str(point)+"\n")
        f.close()
    return render_template('msg.html', msg="成功!")

@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('js', path)

if __name__ == '__main__':
    lock=threading.Lock()
    file_lock=threading.Lock()
    with lock:
        init_player()
    #app.run(debug=True, host='127.0.0.1', port=listenPort, threaded=True)
    app.run(debug=False, host='0.0.0.0', port=listenPort, threaded=True)
